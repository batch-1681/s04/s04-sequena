<?php 

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors =$floors;
        $this->address = $address;
    }
}

class Caswynn extends Building{
    // Encapsulation - getter
    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name =$name;
    }
    public function getFloors(){
        return $this->floors;
    }
    public function getAddress(){
        return $this->address;
    }
    private function setFloor($floors){
        $this->floors =$floors;
    }
    private function setPrivateAddress($address){
        $this->address =$address;
    }
}


 
class Condominium extends Building{
    // Encapsulation - getter
    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name =$name;
    }
    public function getFloors(){
        return $this->floors;
    }
    public function getAddress(){
        return $this->address;
    }
    private function setFloor($floors){
        $this->floors =$floors;
    }
    private function setPrivateAddress($address){
        $this->address =$address;
    }
}



$building = new Caswynn('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');


$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");